# Requirements
Python 3.6, 3.7 or 3.8

Torch 1.4, 1.5, 1.6 or 1.7

# Prepare the environment
$ pip install syft

- This will auto-install PyTorch==1.8 and other dependencies as required
- PyTorch 1.8 is not supported, please uninstall it and install pytorch< 1.8 

$ pip install notebook

# Duet examples

The examples can be played with by launching Jupyter Notebook and navigating to the examples/duet folder

# Try out the tutorials

Duet examples can be found [here](https://gitlab.com/cassie.zaf/pysyft_duet/-/tree/master/examples/duet)

Any new example will be added in the above path.
